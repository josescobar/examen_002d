"""n_motos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from appnmotos import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', views.index),
    path('pagina2/', views.pagina2),
    path('pagina3/', views.pagina3),
    path('pagina4/', views.pagina4),
    path('pagina5/', views.pagina5),
    path('agregar_motocicleta/', views.agregar_motocicleta),
    path('ingreso_motocicleta/', views.ingreso_motocicleta),
    path('actualizar_motocicleta/', views.actualizar_motocicleta),
    path('actualizacion_motocicleta/', views.actualizacion_motocicleta),
    path('eliminar_motocicleta/', views.eliminar_motocicleta),
    path('eliminacion_motocicleta/', views.eliminacion_motocicleta),
    path('buscar/', views.buscar),
]
