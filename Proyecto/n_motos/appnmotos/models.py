from django.db import models
from django import forms

# Create your models here.

class Motocicleta(models.Model):
    marca=models.CharField(max_length=60)
    modelo=models.CharField(max_length=100)
    tipo_motocicleta=models.CharField(max_length=20)
    cilindrada=models.IntegerField()
    num_chasis=models.CharField(max_length=50)

class Cliente(models.Model):
    rut=models.CharField(max_length=12)
    nombre=models.CharField(max_length=60)
    telefono=models.CharField(max_length=12)
    correo=models.EmailField()