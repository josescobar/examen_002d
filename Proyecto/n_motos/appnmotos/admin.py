from django.contrib import admin

# Register your models here.
from appnmotos.models import Motocicleta, Cliente
admin.site.register(Motocicleta)
admin.site.register(Cliente)