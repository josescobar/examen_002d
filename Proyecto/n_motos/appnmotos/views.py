from django.shortcuts import render
from django.http import HttpResponse
from appnmotos.models import Motocicleta

# Create your views here.
#vistas de paginas html
def index(request):
    return render(request,"index.html")

def pagina2(request):
    return render(request,"pagina2.html")

def pagina3(request):
    return render(request,"pagina3.html")

def pagina4(resquet):
    return render(request,"pagina4.html")

def pagina5(request):
    return render(request,"pagina5.html")

def agregar_motocicleta(request):
    return render(request,"agregar_motocicleta.html")

def actualizar_motocicleta(request):
    return render(request,"actualizar_motocicleta.html")

def eliminar_motocicleta(request):
    return render(request,"eliminar_motocicleta.html")

def buscar(request):
    return render(request,"buscar.html")

#vistas de procesos
#Agregar
def ingreso_motocicleta(request):
    marca=request.GET["txt_marca_motocicleta"]
    modelo=request.GET["txt_modelo_motocicleta"]
    tipo_motocicleta=request.GET["txt_tipo_motocicleta"]
    cilindrada=request.GET["txt_cilindrada"]
    num_chasis=request.GET["txt_num_chasis"]
    if len(marca)>0 and len(modelo)>0 and len(tipo_motocicleta)>0 and len(cilindrada)>0 and len(num_chasis)>0:
        pro=Motocicleta(marca=marca,modelo=modelo,tipo_motocicleta=tipo_motocicleta,cilindrada=cilindrada,num_chasis=num_chasis)
        pro.save()
        mensaje="Motocicleta ingresada"
    else:
        mensaje="Motocicleta no ingresada. Faltan datos por ingresar"
    return HttpResponse(mensaje)

#Actualizar
def actualizacion_motocicleta(request):
    if request.GET["txt_id"]:
        id_recibido=request.GET["txt_precio"]
        marca_recibida=request.GET["txt_marca"]
        modelo_recibido=request.GET["txt_marca"]
        tipo_motocicleta_recibido=request.GET["txt_tipo_motocicleta"]
        cilindrada_recibida=request.GET["txt_tipo_motocicleta"]
        num_chasis_recibido=request.GET["txt_tipo_chasis"]
        motocicleta=Motocicleta.object.filter
        if motocicleta:
            pro=Motocicleta.object.get(id=id_recibido)
            pro.marca=marca_recibida
            pro.modelo=modelo_recibido
            pro.tipo_motocicleta=tipo_motocicleta_recibido
            pro.cilindrada=cilindrada_recibida
            pro.num_chasis=num_chasis_recibido
            pro.save
            mensaje="Actualización realizada"
            return HttpResponse(mensaje)
        else:
            mensaje="No existen campos para Actualizar"
            return HttpResponse(mensaje)
    else:
        mensaje="Debe ingrersar un id para realizar una actialización"
        return HttpResponse(mensaje)


#Eliminar
def eliminacion_motocicleta(request):
    if request.GET["txt_id"]:
        id_recibido=request.GET["txt_id"]
        motocicleta=Motocicleta.objects.get(id=id_recibido)
        if motocicleta:
            pro=Motocicleta.objects.get(id=id_recibido)
            pro.delete
            mensaje="Motocicleta eliminada"
        else:
            mensaje="Motocicleta no eliminada. No existe una motocicleta con ese id"
    else:
        mensaje="Debe ingresar un id"
    return HttpResponse(mensaje)


#Listar todos los registros y con 2 filtros adicionales
#-listar
def listar_todo(request):
    motocicletas = Motocicleta.object.all()
    return render(request, "buscar.html",{"motocicletas":motocicletas})
#-Buscar 1
def filtro_marca(request):
    if request.GET["txt_marca"]:
        marca=request.GET["txt_marca"]
        motocicletas=Motocicleta.object.filter(nombre__incontains=marca)
        return render(request, "buscar.html",{"motocicletas":motocicletas, "query":marca})
    else:
        mensaje="Debe ingresar la marca"
    return HttpResponse(mensaje)
#- Buscar 2
def filtro_tipo_motocicleta(request):
    if request.GET["txt_tipo_motocicleta"]:
        tipo_motocicleta=request.GET["txt_tipo_motocicleta"]
        motocicletas=Motocicleta.object.filter(nombre__incontains=tipo_motocicleta)
        return render(request, "buscar.html",{"motocicletas":motocicletas, "query":tipo_motocicleta})
    else:
        mensaje="Debe ingresar el tipo de motocicleta"
    return HttpResponse(mensaje)



